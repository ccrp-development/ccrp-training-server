@ECHO OFF
:choice
set /P c=Would you like to delete your server cache?[Y/N]?
if /I "%c%" EQU "Y" goto :somewhere
if /I "%c%" EQU "N" goto :somewhere_else
goto :choice
:somewhere
echo "Deleting server cache"
rd /s /q "C:\ccrp-training-server\cache\"
echo -
echo Commercial City Roleplay - Pillsbury is GOD
echo -
start C:\ccrp-training-server\run.cmd +exec server.cfg
exit
:somewhere_else
echo
@echo off
echo -
echo Thank you for being apart of Commercial City RP
echo -
start C:\ccrp-training-server\run.cmd +exec server.cfg
exit