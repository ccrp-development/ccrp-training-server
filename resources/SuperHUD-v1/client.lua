------------------------------------------------------------------------
--																	  --
--																	  --
--					  THIS SCRIPT WAS MADE BY MNP :)				  --
--																	  --
--																	  --
------------------------------------------------------------------------

-- CONFIG --
local Colors = {
	
	r = 250,
	g = 140,
	b = 255, 
	a = 205,
}
local Media = {

	discord = 'https://discord.gg/mK2c7XW',
	teamspeak = '45.58.61.101:9004',
}

local HUDServer = { -- This is the name that is shown on the pause screen, (the part that shows the big map.) --

	name = 'CCRP EMS/LEO/FD Training', --Your Server Name
	color = '~p~', --Your Server Color, Hint: https://forum.fivem.net/t/how-to-use-colors-in-lua-scripting/458
}

local HUD = {

	Locationx = 0.662,
	Locationy = 1.42,
}

DisplayTimeandDate = true -- Show Time, Date and AOP Location
DisplayServerLinks = false -- Show Server Links

-- CODE, DO NOT EDIT WITH OUT KNOWING WHAT YOU ARE DOING --
Citizen.CreateThread(function()
  	while true do
		Wait(1)
		local servername = HUDServer.name
		local servercolor = HUDServer.color
		local name = GetPlayerName(PlayerId())
		local id = GetPlayerServerId(id)
			AddTextEntry('FE_THDR_GTAO', servercolor.. '' ..servername..' ~w~| '..servercolor..'Name: ~w~' ..name.. ' ~w~| ' ..servercolor.. 'ID: ~w~'..id..' ~w~| ' ..servercolor.. 'AOP: ~w~'..CurrentAOP..' '..CurrentAOP2)
	end
end)
if(DisplayServerLinks)then
servername = "~n~~n~Discord: ~w~"..Media.discord.." ~n~ ~s~TeamSpeak: ~w~"..Media.teamspeak..""

scale = 0.45

Citizen.CreateThread(function()
	while true do
		Wait(1)
		
		SetTextColour(Colors.r, Colors.g, Colors.b, Colors.a)

		SetTextFont(4)
		SetTextScale(scale, scale)
		SetTextProportional(0)
		SetTextCentre(false)
		SetTextDropshadow(0, 0, 0, 0, 255)
		SetTextEdge(2, 0, 0, 0, 255)
		SetTextEntry("STRING")
		SetTextOutline()
		AddTextComponentString(servername)
		DrawText(0.162, 0.75)
	end
end)
end		





-- CODE FOR VEHICLE HUD --
Citizen.CreateThread(function()
	while true do Citizen.Wait(1)


		local MyPed = GetPlayerPed(-1)
		
		if(IsPedInAnyVehicle(MyPed, false))then

			local MyPedVeh = GetVehiclePedIsIn(GetPlayerPed(-1),false)
			local PlateVeh = GetVehicleNumberPlateText(MyPedVeh)
			local VehStopped = IsVehicleStopped(MyPedVeh)
			local Gear = GetVehicleCurrentGear(MyPedVeh)								-- Check the current gear of the vehicle
            local RPM = GetVehicleCurrentRpm(MyPedVeh)									-- Check the rpm of the vehicle
			Speed = GetEntitySpeed(GetVehiclePedIsIn(GetPlayerPed(-1), false)) * 2.236936 -- Corrects Speed
			if RPM > 0.99 then					--Randomly generate middle rpm digits
				RPM = RPM*100
				RPM = RPM+math.random(-2,2)
				RPM = RPM/100
			end
			
			--drawTxt4(HUD.Locationx + .099, HUD.Locationy - .0008, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | " "~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a) --Draws Speed
			
			if VehStopped and (Speed == 0) then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~r~P ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear < 1 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~r~R ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)						
			elseif Gear == 1 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~1 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 2 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~2 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 3 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~3 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 4 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~4 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 5 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~5 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 6 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~6 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 7 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~7 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 8 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~8 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 9 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~9 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			elseif Gear == 10 then
				drawTxt4(HUD.Locationx, HUD.Locationy - .0005, 1.0,1.0,0.45, "Plate: ~w~"..PlateVeh.."~s~ | Gear: ~w~10 ~s~| ~s~MPH: ~w~" .. math.ceil(Speed) .. " ~s~| RPM: ~w~" ..  math.ceil(round(RPM, 2)*8000) - 700, Colors.r, Colors.g, Colors.b, Colors.a)
			end
		end		
	end
end)

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

function drawTxt4(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(2, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end

-- END CODE FOR VEHICLE HUD --
-- BEGIN CODE FOR TIME AND DATE DISPLAY --

function GetTimeAndMinutes()
	
	gethour=GetClockHours()
	getmins=GetClockMinutes()

	local y,mo,d,h,m,s=GetLocalTime()

	minz=""
		if(getmins<=9)then
			minz="0"
		end
	
	local hour=h
		if(h>12)then
			hour=h-12
		end
		if(h==0)then
			hour="12"
		end
	
	local period="AM"
		if(h>=12)then
			period="PM"
		end

	local minutes=m
		if(m<10)then
			minutes="0"..m..""
		end
	
	
	if(DisplayTimeandDate)then
		drawTDMFPS(string.format("Time: ~w~%s:~w~%s%s~s~ | ~w~%s~s~/~w~%s~s~/~w~%s~s~ | AOP: ~w~"..CurrentAOP.. " " ..CurrentAOP2,gethour,minz,getmins,mo,d,y),1500,12.5,0.46,true,4,191,10,48,205) -- puts the whole thing in the desired location, drawT is a funciton below 	
	end
end
Citizen.CreateThread(function()
	while true do
		Wait(0)
	
		local ped_l=GetPlayerPed(-1)
	
		if(DoesEntityExist(ped_l))then
			GetTimeAndMinutes()
		end
	end
end)
function drawTDMFPS(text, x, y, size, center, font, r, g, b, a)
local resx,resy=GetScreenResolution() -- determines resoultion using native
	SetTextFont(font)
	SetTextScale(size,size)
	SetTextProportional(true)
	SetTextColour(Colors.r, Colors.g, Colors.b, Colors.a)
	SetTextCentre(center)
	SetTextDropshadow(0,0,0,0,0)
	SetTextOutline()
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(HUD.Locationx - 0.418, HUD.Locationy - 0.471) 
end
-- BEGIN CODE FOR STREET LABEL --

x = 0.99
y = 0.94

town_r = 255
town_g = 255
town_b = 255
town_a = 255

str_around_r = 255
str_around_g = 255 
str_around_b = 255
str_around_a = 255

border_r = 255
border_g = 255
border_b = 255
border_a = 100

function drawTxt(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(4)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end
function drawTxt2(x,y ,width,height,scale, text, r,g,b,a)
    SetTextFont(6)
    SetTextProportional(0)
    SetTextScale(scale, scale)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - width/2, y - height/2 + 0.005)
end
local zones = { ['AIRP'] = "Los Santos International Airport", ['ALAMO'] = "Alamo Sea", ['ALTA'] = "Alta", ['ARMYB'] = "Fort Zancudo", ['BANHAMC'] = "Banham Canyon Dr", ['BANNING'] = "Banning", ['BEACH'] = "Vespucci Beach", ['BHAMCA'] = "Banham Canyon", ['BRADP'] = "Braddock Pass", ['BRADT'] = "Braddock Tunnel", ['BURTON'] = "Burton", ['CALAFB'] = "Calafia Bridge", ['CANNY'] = "Raton Canyon", ['CCREAK'] = "Cassidy Creek", ['CHAMH'] = "Chamberlain Hills", ['CHIL'] = "Vinewood Hills", ['CHU'] = "Chumash", ['CMSW'] = "Chiliad Mountain State Wilderness", ['CYPRE'] = "Cypress Flats", ['DAVIS'] = "Davis", ['DELBE'] = "Del Perro Beach", ['DELPE'] = "Del Perro", ['DELSOL'] = "La Puerta", ['DESRT'] = "Grand Senora Desert", ['DOWNT'] = "Downtown", ['DTVINE'] = "Downtown Vinewood", ['EAST_V'] = "East Vinewood", ['EBURO'] = "El Burro Heights", ['ELGORL'] = "El Gordo Lighthouse", ['ELYSIAN'] = "Elysian Island", ['GALFISH'] = "Galilee", ['GOLF'] = "GWC and Golfing Society", ['GRAPES'] = "Grapeseed", ['GREATC'] = "Great Chaparral", ['HARMO'] = "Harmony", ['HAWICK'] = "Hawick", ['HORS'] = "Vinewood Racetrack", ['HUMLAB'] = "Humane Labs and Research", ['JAIL'] = "Bolingbroke Penitentiary", ['KOREAT'] = "Little Seoul", ['LACT'] = "Land Act Reservoir", ['LAGO'] = "Lago Zancudo", ['LDAM'] = "Land Act Dam", ['LEGSQU'] = "Legion Square", ['LMESA'] = "La Mesa", ['LOSPUER'] = "La Puerta", ['MIRR'] = "Mirror Park", ['MORN'] = "Morningwood", ['MOVIE'] = "Richards Majestic", ['MTCHIL'] = "Mount Chiliad", ['MTGORDO'] = "Mount Gordo", ['MTJOSE'] = "Mount Josiah", ['MURRI'] = "Murrieta Heights", ['NCHU'] = "North Chumash", ['NOOSE'] = "N.O.O.S.E", ['OCEANA'] = "Pacific Ocean", ['PALCOV'] = "Paleto Cove", ['PALETO'] = "Paleto Bay", ['PALFOR'] = "Paleto Forest", ['PALHIGH'] = "Palomino Highlands", ['PALMPOW'] = "Palmer-Taylor Power Station", ['PBLUFF'] = "Pacific Bluffs", ['PBOX'] = "Pillbox Hill", ['PROCOB'] = "Procopio Beach", ['RANCHO'] = "Rancho", ['RGLEN'] = "Richman Glen", ['RICHM'] = "Richman", ['ROCKF'] = "Rockford Hills", ['RTRAK'] = "Redwood Lights Track", ['SANAND'] = "San Andreas", ['SANCHIA'] = "San Chianski Mountain Range", ['SANDY'] = "Sandy Shores", ['SKID'] = "Mission Row", ['SLAB'] = "Stab City", ['STAD'] = "Maze Bank Arena", ['STRAW'] = "Strawberry", ['TATAMO'] = "Tataviam Mountains", ['TERMINA'] = "Terminal", ['TEXTI'] = "Textile City", ['TONGVAH'] = "Tongva Hills", ['TONGVAV'] = "Tongva Valley", ['VCANA'] = "Vespucci Canals", ['VESP'] = "Vespucci", ['VINE'] = "Vinewood", ['WINDF'] = "Ron Alternates Wind Farm", ['WVINE'] = "West Vinewood", ['ZANCUDO'] = "Zancudo River", ['ZP_ORT'] = "Port of South Los Santos", ['ZQ_UAR'] = "Davis Quartz" }
local directions = { [0] = 'N', [45] = 'NW', [90] = 'W', [135] = 'SW', [180] = 'S', [225] = 'SE', [270] = 'E', [315] = 'NE', [360] = 'N', } 
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		local pos = GetEntityCoords(GetPlayerPed(-1))
		local var1, var2 = GetStreetNameAtCoord(pos.x, pos.y, pos.z, Citizen.ResultAsInteger(), Citizen.ResultAsInteger())
		local current_zone = zones[GetNameOfZone(pos.x, pos.y, pos.z)]

		for k,v in pairs(directions)do
			direction = GetEntityHeading(GetPlayerPed(-1))
			if(math.abs(direction - k) < 22.5)then
				direction = v
				break;
			end
		end
		
		if(GetStreetNameFromHashKey(var1) and GetNameOfZone(pos.x, pos.y, pos.z))then
			if(zones[GetNameOfZone(pos.x, pos.y, pos.z)] and tostring(GetStreetNameFromHashKey(var1)))then
				if direction == 'N' then
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.306, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.285, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.285, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
						drawTxt2(x-0.285, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'NE' then 
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.298, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.277, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.277, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
					drawTxt2(x-0.277, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)),Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'E' then 
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.309, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.288, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.288, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
					drawTxt2(x-0.288, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'SE' then 
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.298, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.275, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.275, x+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
						drawTxt2(x-0.275, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'S' then
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.307, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.285, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.285, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
						drawTxt2(x-0.285, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'SW' then
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.292, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.270, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.270, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
						drawTxt2(x-0.270, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'W' then 
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.303, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then 
						drawTxt2(x-0.280, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else
						drawTxt2(x-0.280, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end
						drawTxt2(x-0.280, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				elseif direction == 'NW' then
						drawTxt(x-0.335, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.290, y+0.66, 1.0,1.5,1.4, " | ", border_r, border_g, border_b, border_a)
						drawTxt(x-0.315, y+0.42, 1.0,1.0,1.0, direction, Colors.r, Colors.g, Colors.b, Colors.a)
					if tostring(GetStreetNameFromHashKey(var2)) == "" then
						drawTxt2(x-0.266, y+0.45, 1.0,1.0,0.45, current_zone, town_r, town_g, town_b, town_a)
					else 
						drawTxt2(x-0.266, y+0.45, 1.0,1.0,0.45, tostring(GetStreetNameFromHashKey(var2)) .. ", " .. zones[GetNameOfZone(pos.x, pos.y, pos.z)], str_around_r, str_around_g, str_around_b, str_around_a)
					end 
						drawTxt2(x-0.266, y+0.42, 1.0,1.0,0.55, tostring(GetStreetNameFromHashKey(var1)), Colors.r, Colors.g, Colors.b, Colors.a)
				end
			end
		end
	end
end)
-- END STREET LABEL --
-- BEGIN AOP --
CurrentAOP = "San"
CurrentAOP2 = "Andreas"

AddEventHandler('playerSpawned', function()
    TriggerServerEvent('aop:requestSync')
end)

RegisterNetEvent('aop:updateAOP')
AddEventHandler('aop:updateAOP', function(newCurrentAOP, newCurrentAOP2)
    CurrentAOP = newCurrentAOP
	CurrentAOP2 = newCurrentAOP2
end)

-- END AOP --
------------------------------------------------------------------------
--																	  --
--																	  --
--					 THIS SCRIPT WAS MADE BY MNP :)	 				  --
--																	  --
--																	  --
------------------------------------------------------------------------